"use strict"

import Authentication from "../modules/authentication.js"
import Logger from "../modules/logger.js"
import PopupItemList from "../modules/popup_item_list.js"
import PopupMainLoader from "../modules/popup_main_loader.js"
import PopupPagination from "../modules/popup_pagination.js"
import PopupTopActions from "../modules/popup_top_actions.js"
import PopupTopFilter from "../modules/popup_top_filter.js"
import Settings from "../modules/settings.js"
import Utility from "../modules/utility.js"
import { VersionManager } from "../modules/version_manager.js"
import { PopupFlash } from "../modules/popup_flash.js"

// ----------------

const defaultDisplaySetting = { currentPage: 1, query: "" }

const PopupUI = (function () {
  const intervalWithoutOpening = 5 * 60

  function setupEventListeners() {
    PopupPagination.setupEventListeners()
    PopupTopActions.setupEventListeners()
    PopupTopFilter.setupEventListeners()
    PopupItemList.setupEventListeners()
  }

  function setZoomLevel() {
    Settings.init().then(function () {
      const zoomLevel = Settings.get("zoomLevel")
      document.documentElement.style.fontSize = zoomLevel
    })
  }

  function setupAuthenticatedUI() {
    Logger.log("(PopupUI.setupAuthenticatedUI)")

    // User is authenticated
    document.querySelector(".authentication").classList.add("hidden")
    document.querySelector(".authenticated").classList.remove("hidden")

    // Show pagination if setting is enabled
    PopupPagination.setPaginationVisibility()

    // Set up the event listeners on the UI
    setupEventListeners()

    browser.storage.local.get("display").then(({ display }) => {
      const currentTimestamp = (Date.now() / 1000) | 0
      const parsedDisplay = Utility.parseJson(display) || defaultDisplaySetting
      const lastDisplay = parsedDisplay.displayedAt

      const displayOptions = Object.assign({}, parsedDisplay)

      // Reset query and currentPage if more than `intervalWithoutOpening` since last opening
      if (lastDisplay && currentTimestamp - lastDisplay > intervalWithoutOpening) {
        Logger.log("(PopupUI.setupAuthenticatedUI) reset page to 1 and filter to ''")
        Object.assign(displayOptions, defaultDisplaySetting)
      }

      // Set initial filter value in the PopupUI and focus the field
      PopupTopFilter.setValue(displayOptions.query)
      PopupTopFilter.updateFavoriteFilterIcon()
      PopupTopFilter.focusSearchField()

      // Updates display.displayedAt and page + query if they have been reset
      Object.assign(displayOptions, { displayedAt: currentTimestamp })
      Logger.log("(PopupUI.setupAuthenticatedUI) Save display variable to local storage: " + displayOptions)
      browser.storage.local.set({ display: JSON.stringify(displayOptions) })
    })
  }

  function setupUnauthenticatedUI() {
    Logger.log("(PopupUI.setupUnauthenticatedUI)")

    // User is not authenticated yet
    const authenticationButton = document.querySelector(".authentication button")
    const pocketSignupLink = document.querySelector(".authentication .signup")

    document.querySelector(".authentication").classList.remove("hidden")
    document.querySelector(".authenticated").classList.add("hidden")

    authenticationButton.addEventListener("click", () => {
      browser.runtime.sendMessage({ action: "authenticate" })
    })

    pocketSignupLink.addEventListener("click", () => {
      setTimeout(() => {
        window.close()
      }, 200)
    })
  }

  function ensureFullResyncTriggeredIfNeeded() {
    browser.storage.local
      .get(["access_token", "lastFullSyncAtVersion"])
      .then(({ access_token, lastFullSyncAtVersion }) => {
        if (access_token && VersionManager.mustTriggerFullResync(lastFullSyncAtVersion)) {
          PopupFlash.showNeedResyncMessage()
        }
      })
  }

  return {
    inSidebar: () => window.location.search.includes("ui=sidebar"),
    inPopup: () => window.location.search.includes("ui=popup"),

    setup: function () {
      setZoomLevel()

      Authentication.isAuthenticated().then(
        () => {
          setupAuthenticatedUI()
          ensureFullResyncTriggeredIfNeeded()
          PopupItemList.drawList()

          setTimeout(() => {
            PopupMainLoader.enable()
            browser.runtime.sendMessage({ action: "retrieve-items", force: false })
          }, 1000)
        },
        () => {
          setupUnauthenticatedUI()
        }
      )
    },
  }
})()

export { defaultDisplaySetting }
export default PopupUI
